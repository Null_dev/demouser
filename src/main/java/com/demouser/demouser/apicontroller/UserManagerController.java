package com.demouser.demouser.apicontroller;


import javax.validation.Valid;

import com.demouser.demouser.model.User;
import com.demouser.demouser.service.UserManagerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserManagerController {
	@Autowired
	private UserManagerService userManagerService;

	@PostMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> add(@RequestBody @Valid User user, Errors errors) {
		if(errors.hasErrors()) {
			return new ResponseEntity<>(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(userManagerService.addUser(user), HttpStatus.CREATED);
	}
}