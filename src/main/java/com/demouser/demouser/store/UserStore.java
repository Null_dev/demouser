package com.demouser.demouser.store;

import com.demouser.demouser.model.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserStore extends CrudRepository<User, String>{
    
}